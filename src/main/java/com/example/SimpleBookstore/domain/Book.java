package com.example.SimpleBookstore.domain;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private double price;
    private int quantity;

    @ManyToOne
    private Author author;

    @ManyToOne
    private Genre genre;
}