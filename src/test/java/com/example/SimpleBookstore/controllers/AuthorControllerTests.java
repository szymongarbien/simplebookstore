package com.example.SimpleBookstore.controllers;

import com.example.SimpleBookstore.domain.Author;
import com.example.SimpleBookstore.repositories.AuthorRepository;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class AuthorControllerTests {

    @Test
    void createAuthor() {
        // Arrange
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        AuthorController authorController = new AuthorController();
        authorController.setAuthorRepository(authorRepository);

        Author inputAuthor = new Author();
        Author savedAuthor = new Author();
        when(authorRepository.save(inputAuthor)).thenReturn(savedAuthor);

        // Act
        ResponseEntity<Author> response = authorController.createAuthor(inputAuthor);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(savedAuthor, response.getBody());
    }

    @Test
    void getAllAuthors() {
        // Arrange
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        AuthorController authorController = new AuthorController();
        authorController.setAuthorRepository(authorRepository);

        List<Author> authors = Arrays.asList(new Author(), new Author());
        when(authorRepository.findAll()).thenReturn(authors);

        // Act
        ResponseEntity<List<Author>> response = authorController.getAllAuthors();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(authors, response.getBody());
    }

    @Test
    void getAuthorById_ExistingId_ReturnsAuthor() {
        // Arrange
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        AuthorController authorController = new AuthorController();
        authorController.setAuthorRepository(authorRepository);

        Long existingId = 1L;
        Author existingAuthor = new Author();
        when(authorRepository.findById(existingId)).thenReturn(Optional.of(existingAuthor));

        // Act
        ResponseEntity<Author> response = authorController.getAuthorById(existingId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(existingAuthor, response.getBody());
    }

    @Test
    void getAuthorById_NonExistingId_ReturnsNotFound() {
        // Arrange
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        AuthorController authorController = new AuthorController();
        authorController.setAuthorRepository(authorRepository);

        Long nonExistingId = 1L;
        when(authorRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<Author> response = authorController.getAuthorById(nonExistingId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void updateAuthor_ExistingId_ReturnsUpdatedAuthor() {
        // Arrange
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        AuthorController authorController = new AuthorController();
        authorController.setAuthorRepository(authorRepository);

        Long existingId = 1L;
        Author existingAuthor = new Author();
        when(authorRepository.existsById(existingId)).thenReturn(true);
        when(authorRepository.save(any(Author.class))).thenReturn(existingAuthor);

        // Act
        ResponseEntity<Author> response = authorController.updateAuthor(existingId, new Author());

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(existingAuthor, response.getBody());
    }

    @Test
    void updateAuthor_NonExistingId_ReturnsNotFound() {
        // Arrange
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        AuthorController authorController = new AuthorController();
        authorController.setAuthorRepository(authorRepository);

        Long nonExistingId = 1L;
        when(authorRepository.existsById(nonExistingId)).thenReturn(false);

        // Act
        ResponseEntity<Author> response = authorController.updateAuthor(nonExistingId, new Author());

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void deleteAuthor_ExistingId_ReturnsNoContent() {
        // Arrange
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        AuthorController authorController = new AuthorController();
        authorController.setAuthorRepository(authorRepository);

        Long existingId = 1L;
        when(authorRepository.existsById(existingId)).thenReturn(true);

        // Act
        ResponseEntity<Void> response = authorController.deleteAuthor(existingId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(authorRepository, times(1)).deleteById(existingId);
    }

    @Test
    void deleteAuthor_NonExistingId_ReturnsNotFound() {
        // Arrange
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        AuthorController authorController = new AuthorController();
        authorController.setAuthorRepository(authorRepository);

        Long nonExistingId = 1L;
        when(authorRepository.existsById(nonExistingId)).thenReturn(false);

        // Act
        ResponseEntity<Void> response = authorController.deleteAuthor(nonExistingId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(authorRepository, never()).deleteById(nonExistingId);
    }

    // Additional tests can be added...
}
