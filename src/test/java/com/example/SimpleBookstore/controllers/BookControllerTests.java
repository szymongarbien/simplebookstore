package com.example.SimpleBookstore.controllers;

import com.example.SimpleBookstore.domain.Book;
import com.example.SimpleBookstore.repositories.BookRepository;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class BookControllerTests {

    @Test
    void createBook() {
        // Arrange
        BookRepository bookRepository = mock(BookRepository.class);
        BookController bookController = new BookController();
        bookController.setBookRepository(bookRepository);

        Book inputBook = new Book();
        Book savedBook = new Book();
        when(bookRepository.save(inputBook)).thenReturn(savedBook);

        // Act
        ResponseEntity<Book> response = bookController.createBook(inputBook);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(savedBook, response.getBody());
    }

    @Test
    void getAllBooks() {
        // Arrange
        BookRepository bookRepository = mock(BookRepository.class);
        BookController bookController = new BookController();
        bookController.setBookRepository(bookRepository);

        List<Book> books = Arrays.asList(new Book(), new Book());
        when(bookRepository.findAll()).thenReturn(books);

        // Act
        ResponseEntity<List<Book>> response = bookController.getAllBooks();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(books, response.getBody());
    }

    @Test
    void getBookById_ExistingId_ReturnsBook() {
        // Arrange
        BookRepository bookRepository = mock(BookRepository.class);
        BookController bookController = new BookController();
        bookController.setBookRepository(bookRepository);

        Long existingId = 1L;
        Book existingBook = new Book();
        when(bookRepository.findById(existingId)).thenReturn(Optional.of(existingBook));

        // Act
        ResponseEntity<Book> response = bookController.getBookById(existingId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(existingBook, response.getBody());
    }

    @Test
    void getBookById_NonExistingId_ReturnsNotFound() {
        // Arrange
        BookRepository bookRepository = mock(BookRepository.class);
        BookController bookController = new BookController();
        bookController.setBookRepository(bookRepository);

        Long nonExistingId = 1L;
        when(bookRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<Book> response = bookController.getBookById(nonExistingId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void updateBook_ExistingId_ReturnsUpdatedBook() {
        // Arrange
        BookRepository bookRepository = mock(BookRepository.class);
        BookController bookController = new BookController();
        bookController.setBookRepository(bookRepository);

        Long existingId = 1L;
        Book existingBook = new Book();
        when(bookRepository.existsById(existingId)).thenReturn(true);
        when(bookRepository.save(any(Book.class))).thenReturn(existingBook);

        // Act
        ResponseEntity<Book> response = bookController.updateBook(existingId, new Book());

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(existingBook, response.getBody());
    }

    @Test
    void updateBook_NonExistingId_ReturnsNotFound() {
        // Arrange
        BookRepository bookRepository = mock(BookRepository.class);
        BookController bookController = new BookController();
        bookController.setBookRepository(bookRepository);

        Long nonExistingId = 1L;
        when(bookRepository.existsById(nonExistingId)).thenReturn(false);

        // Act
        ResponseEntity<Book> response = bookController.updateBook(nonExistingId, new Book());

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void deleteBook_ExistingId_ReturnsNoContent() {
        // Arrange
        BookRepository bookRepository = mock(BookRepository.class);
        BookController bookController = new BookController();
        bookController.setBookRepository(bookRepository);

        Long existingId = 1L;
        when(bookRepository.existsById(existingId)).thenReturn(true);

        // Act
        ResponseEntity<Void> response = bookController.deleteBook(existingId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(bookRepository, times(1)).deleteById(existingId);
    }

    @Test
    void deleteBook_NonExistingId_ReturnsNotFound() {
        // Arrange
        BookRepository bookRepository = mock(BookRepository.class);
        BookController bookController = new BookController();
        bookController.setBookRepository(bookRepository);

        Long nonExistingId = 1L;
        when(bookRepository.existsById(nonExistingId)).thenReturn(false);

        // Act
        ResponseEntity<Void> response = bookController.deleteBook(nonExistingId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(bookRepository, never()).deleteById(nonExistingId);
    }

    // Additional tests for search methods can be added similarly...
}
