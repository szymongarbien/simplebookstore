package com.example.SimpleBookstore.controllers;

import com.example.SimpleBookstore.domain.Genre;
import com.example.SimpleBookstore.repositories.GenreRepository;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class GenreControllerTests {

    @Test
    void createGenre() {
        // Arrange
        GenreRepository genreRepository = mock(GenreRepository.class);
        GenreController genreController = new GenreController();
        genreController.setGenreRepository(genreRepository);

        Genre inputGenre = new Genre();
        Genre savedGenre = new Genre();
        when(genreRepository.save(inputGenre)).thenReturn(savedGenre);

        // Act
        ResponseEntity<Genre> response = genreController.createGenre(inputGenre);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(savedGenre, response.getBody());
    }

    @Test
    void getAllGenres() {
        // Arrange
        GenreRepository genreRepository = mock(GenreRepository.class);
        GenreController genreController = new GenreController();
        genreController.setGenreRepository(genreRepository);

        List<Genre> genres = Arrays.asList(new Genre(), new Genre());
        when(genreRepository.findAll()).thenReturn(genres);

        // Act
        ResponseEntity<List<Genre>> response = genreController.getAllGenres();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(genres, response.getBody());
    }

    @Test
    void getGenreById_ExistingId_ReturnsGenre() {
        // Arrange
        GenreRepository genreRepository = mock(GenreRepository.class);
        GenreController genreController = new GenreController();
        genreController.setGenreRepository(genreRepository);

        Long existingId = 1L;
        Genre existingGenre = new Genre();
        when(genreRepository.findById(existingId)).thenReturn(Optional.of(existingGenre));

        // Act
        ResponseEntity<Genre> response = genreController.getGenreById(existingId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(existingGenre, response.getBody());
    }

    @Test
    void getGenreById_NonExistingId_ReturnsNotFound() {
        // Arrange
        GenreRepository genreRepository = mock(GenreRepository.class);
        GenreController genreController = new GenreController();
        genreController.setGenreRepository(genreRepository);

        Long nonExistingId = 1L;
        when(genreRepository.findById(nonExistingId)).thenReturn(Optional.empty());

        // Act
        ResponseEntity<Genre> response = genreController.getGenreById(nonExistingId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void updateGenre_ExistingId_ReturnsUpdatedGenre() {
        // Arrange
        GenreRepository genreRepository = mock(GenreRepository.class);
        GenreController genreController = new GenreController();
        genreController.setGenreRepository(genreRepository);

        Long existingId = 1L;
        Genre existingGenre = new Genre();
        when(genreRepository.existsById(existingId)).thenReturn(true);
        when(genreRepository.save(any(Genre.class))).thenReturn(existingGenre);

        // Act
        ResponseEntity<Genre> response = genreController.updateGenre(existingId, new Genre());

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(existingGenre, response.getBody());
    }

    @Test
    void updateGenre_NonExistingId_ReturnsNotFound() {
        // Arrange
        GenreRepository genreRepository = mock(GenreRepository.class);
        GenreController genreController = new GenreController();
        genreController.setGenreRepository(genreRepository);

        Long nonExistingId = 1L;
        when(genreRepository.existsById(nonExistingId)).thenReturn(false);

        // Act
        ResponseEntity<Genre> response = genreController.updateGenre(nonExistingId, new Genre());

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void deleteGenre_ExistingId_ReturnsNoContent() {
        // Arrange
        GenreRepository genreRepository = mock(GenreRepository.class);
        GenreController genreController = new GenreController();
        genreController.setGenreRepository(genreRepository);

        Long existingId = 1L;
        when(genreRepository.existsById(existingId)).thenReturn(true);

        // Act
        ResponseEntity<Void> response = genreController.deleteGenre(existingId);

        // Assert
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        verify(genreRepository, times(1)).deleteById(existingId);
    }

    @Test
    void deleteGenre_NonExistingId_ReturnsNotFound() {
        // Arrange
        GenreRepository genreRepository = mock(GenreRepository.class);
        GenreController genreController = new GenreController();
        genreController.setGenreRepository(genreRepository);

        Long nonExistingId = 1L;
        when(genreRepository.existsById(nonExistingId)).thenReturn(false);

        // Act
        ResponseEntity<Void> response = genreController.deleteGenre(nonExistingId);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        verify(genreRepository, never()).deleteById(nonExistingId);
    }

    // Additional tests can be added...
}
