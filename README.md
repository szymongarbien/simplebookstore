# Simple Bookstore Application

This is a simple online bookstore application built with Spring Boot, Hibernate, and MySQL.

## Table of Contents

- [Features](#features)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [API Endpoints](#api-endpoints)
- [Testing with Postman](#testing-with-postman)
- [Feedback](#feedback)

## Features

- Create, read, update, and delete books, authors, and genres.
- Store book-related information in a MySQL database using Hibernate.
- RESTful API endpoints for managing books, authors, and genres.

## Prerequisites

Before running the application, make sure you have the following installed:

- Java Development Kit (JDK)
- Gradle Build Tool
- MySQL Database

## Getting Started

1. Clone the repository:

   ```bash
   git clone https://github.com/your-username/simple-bookstore.git
   ```

2. Navigate to the project directory:

   ```bash
   cd simple-bookstore
   ```

3. Configure the database:

   Open `src/main/resources/application.properties`

   Update the database connection properties:

   ```properties
   spring.datasource.url=jdbc:mysql://localhost:3306/bookstore_db
   spring.datasource.username=your-username
   spring.datasource.password=your-password
   ```

4. Run the application:

   ```bash
   ./gradlew bootRun
   ```

   The application will start on http://localhost:8080.

## API Endpoints

### Books

#### Get All Books

Endpoint: GET /api/books

Description: Retrieve a list of all books.

#### Get Book by ID

Endpoint: GET /api/books/{id}

Description: Retrieve a book by its ID.

Example: GET /api/books/1

#### Create Book

Endpoint: POST /api/books

Description: Create a new book.

Request Body:
```json
{
  "title": "Example Book",
  "author": "Example Author",
  "genre": "Example Genre",
  "price": 19.99,
  "quantity": 100
}
```

#### Update Book

Endpoint: PUT /api/books/{id}

Description: Update an existing book.

Request Body:
```json
{
  "title": "Updated Book",
  "author": "Updated Author",
  "genre": "Updated Genre",
  "price": 29.99,
  "quantity": 50
}
```

Example: PUT /api/books/1

#### Delete Book

Endpoint: DELETE /api/books/{id}

Description: Delete a book by its ID.

Example: DELETE /api/books/1

#### Search for Books

Endpoint: GET /api/books/search/title

Description: Search for books by title.

Example: GET /api/books/search/title?title=Example

Endpoint: GET /api/books/search/author

Description: Search for books by author.

Example: GET /api/books/search/author?authorName=Example

Endpoint: GET /api/books/search/genre

Description: Search for books by genre.

Example: GET /api/books/search/genre?genreName=Example

### Authors

#### Get All Authors

Endpoint: GET /api/authors

Description: Retrieve a list of all authors.

#### Get Author by ID

Endpoint: GET /api/authors/{id}

Description: Retrieve an author by ID.

Example: GET /api/authors/1

#### Create Author

Endpoint: POST /api/authors

Description: Create a new author.

Request Body:
```json
{
  "name": "Example Author"
}
```

#### Update Author

Endpoint: PUT /api/authors/{id}

Description: Update an existing author.

Request Body:
```json
{
  "name": "Updated Author"
}
```

Example: PUT /api/authors/1

#### Delete Author

Endpoint: DELETE /api/authors/{id}

Description: Delete an author by ID.

Example: DELETE /api/authors/1

### Genres

#### Get All Genres

Endpoint: GET /api/genres

Description: Retrieve a list of all genres.

#### Get Genre by ID

Endpoint: GET /api/genres/{id}

Description: Retrieve a genre by ID.

Example: GET /api/genres/1

#### Create Genre

Endpoint: POST /api/genres

Description: Create a new genre.

Request Body:
```json
{
  "name": "Example Genre"
}
```

#### Update Genre

Endpoint: PUT /api/genres/{id}

Description: Update an existing genre.

Request Body:
```json
{
  "name": "Updated Genre"
}
```

Example: PUT /api/genres/1

#### Delete Genre

Endpoint: DELETE /api/genres/{id}

Description: Delete a genre by ID.

Example: DELETE /api/genres/1

## Testing with Postman

You can use Postman to test the API endpoints.

## Feedback

- Was it easy to complete the task using AI?

  This one went really well, turns out that giving more detailed prompts gives much better results.


- How long did task take you to complete? (Please be honest, we need it to gather anonymized statistics)

  1.5h total.


- Was the code ready to run after generation? What did you have to change to make it usable?

  Almost everything was AI generated.


- Which challenges did you face during completion of the task?

  This, being my second attempt, turned out very easy.


- Which specific prompts you learned as a good practice to complete the task?

  Giving a very concrete example and asking for a rework is the way to go.
